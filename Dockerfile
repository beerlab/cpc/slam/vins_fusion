ARG BASE_IMG='ubuntu:18.04'

FROM ${BASE_IMG}

SHELL ["/bin/bash", "-ci"]
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y \
	gnupg2 curl wget lsb-core vim \
	build-essential software-properties-common \
	python-pip \
	cmake libgoogle-glog-dev libgflags-dev \
	libatlas-base-dev libeigen3-dev libsuitesparse-dev \
	libpng16-16 libjpeg-turbo8 libtiff5 && \
    rm -rf /var/lib/apt/lists/*

# Install ros melodic
RUN echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list && \
    curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | apt-key add - && \
    apt update && apt install -y ros-melodic-desktop && \
    echo "source /opt/ros/melodic/setup.bash">> ~/.bashrc && \
	echo "source /workspace/ros_ws/devel/setup.bash">> ~/.bashrc && \
    apt update && apt install -y python-rosdep python-rosinstall python-rosinstall-generator \
        python-wstool python-catkin-tools python-lxml python-catkin-tools && \
    rosdep init && rosdep update && \
    rm -rf /var/lib/apt/lists/*

RUN mkdir ceres && cd ceres &&  wget http://ceres-solver.org/ceres-solver-2.1.0.tar.gz && \
	tar zxf ceres-solver-2.1.0.tar.gz
RUN cd ceres &&  mkdir ceres-bin && cd ceres-bin && \
	cmake ../ceres-solver-2.1.0 && make -j$(nproc) && make install

# Opencv
RUN mkdir ocv && cd ocv && git clone -b 3.4 https://github.com/opencv/opencv.git && \
	git clone -b 3.4 https://github.com/opencv/opencv_contrib.git 
	
RUN cd ocv/opencv && mkdir build && cd build && \
	cmake -D CMAKE_BUILD_TYPE=Release -D CMAKE_INSTALL_PREFIX=/usr/local \
	-D OPENCV_EXTRA_MODULES_PATH=/ocv/opencv_contrib/modules .. && \
	make -j$(nproc)

RUN cd ocv/opencv/build/doc/ && make -j$(nproc) && cd .. && make install

# Build VINS-fusion
WORKDIR /workspace/ros_ws
RUN mkdir src && cd src && git clone https://github.com/HKUST-Aerial-Robotics/VINS-Fusion.git
RUN catkin_make