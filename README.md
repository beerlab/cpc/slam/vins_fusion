# VINS ROS docker
## Установка и запуск
Конфиги vins - в папке config. Далее:
```
docker compose up
```
##
Изменить видео поток можно в docker-compose.yml, изначальное значение

    video_device:=/dev/video0


## Инфо
Используемый репозиторий: [VINS FUSION](https://github.com/HKUST-Aerial-Robotics/VINS-Fusion)

